import Index from './content/Index';
import { AuthProvider } from './content/AuthProvider';

export default function App() {
  return (
    <>
      <AuthProvider>
        <Index />
      </AuthProvider>
    </>
  );
}