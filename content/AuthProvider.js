import React, { useState } from 'react';

export const AuthContext = React.createContext();

export const AuthProvider = ({ children }) => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [idSelected, setIdSelected] = useState('');
  const [totalPrice, setTotalPrice] = useState(0);

  return (
    <AuthContext.Provider value={{ isLoggedIn, setIsLoggedIn, username, setUsername, password, setPassword,
      idSelected, setIdSelected,
      totalPrice, setTotalPrice }}>
      {children}
    </AuthContext.Provider>
  );
};