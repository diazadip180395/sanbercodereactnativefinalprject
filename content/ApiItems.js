import { client } from "./client";

export const ItemsGet = async () => client.get(`/products?limit=10`);

export const ItemsGetDetail = async (id) => client.get(`/products/${id}`);

// export const newsAdd = async (title, value) =>
// 	client.post(`/news`, { title, value });

// export const newsEdit = async (id, title, value) =>
// 	client.put(`/news/${id}`, { title, value });

// export const newsDelete = async (id) => client.delete(`/news/${id}`);
