import React, { useContext, useEffect, useState } from "react";
import { View, Text, TextInput, TouchableOpacity, StyleSheet, Image, Button } from "react-native";
import { AuthContext } from "./AuthProvider";

import AsyncStorage, {
    useAsyncStorage,
  } from '@react-native-async-storage/async-storage';

export default function AboutScreen() {
    const { isLoggedIn, setIsLoggedIn, username, setUsername, password, setPassword } = useContext(AuthContext);
    // const logout = async () => {
    //   await AsyncStorage.removeItem('@token');
    //   setIsLoggedIn(false);
    // };
    // const email = "tes";
    return(
        <>
        {/* <AuthProvider> */}
            <View style={styles.containerUtama}>
                <Text style={styles.textOuter}>Tentang Saya</Text>
                <View style={styles.containerContent}>
                    <Text style={styles.textTitle}>Tentang Saya</Text>
                    <View style={styles.containerName}>
                        <Image
                            style = {styles.icon}
                            source = {require('./assets/contact.png')}
                        />
                        <View>
                            <Text style={{fontSize : 25, fontWeight : 'bold'}}>{username}</Text>
                            <Text>React Native Developer</Text>
                        </View>
                    </View>
                    <View style={styles.containerDetail}>
                        <Text>PORTOFOLIO</Text>
                        <Text>Media Sosial</Text>
                        <View style={styles.containerIcon}>
                            <Image
                                style = {styles.icon}
                                source = {require('./assets/facebook.png')}
                            />
                            <Image
                                style = {styles.icon}
                                source = {require('./assets/instagram.png')}
                            />  
                            <Image
                                style = {styles.icon}
                                source = {require('./assets/linkedin-logo.png')}
                            />  
                        </View> 
                        <Text>Project</Text>   
                        <View style={styles.containerIcon}>
                            <Image
                                style = {styles.icon}
                                source = {require('./assets/github.png')}
                            />
                            <Image
                                style = {styles.icon}
                                source = {require('./assets/gitlab.png')}
                            />  
                        </View>  
                        <Text>Kemampuan</Text>
                        <Text>Kategori : Bahasa Pemrograman</Text>
                        <View style={styles.containerIcon}>
                            <Image
                                    style = {styles.icon}
                                    source = {require('./assets/python.png')}
                                />
                            <Text>Python</Text>
                            <Image style = {styles.iconSmall} source = {require('./assets/star.png')}/>
                            <Image style = {styles.iconSmall} source = {require('./assets/star.png')}/>
                            <Image style = {styles.iconSmall} source = {require('./assets/star.png')}/>
                            <Image style = {styles.iconSmall} source = {require('./assets/starBlank.png')}/>
                            <Image style = {styles.iconSmall} source = {require('./assets/starBlank.png')}/>
                        </View>
                        <View style={styles.containerIcon}>
                            <Image
                                    style = {styles.icon}
                                    source = {require('./assets/js.png')}
                                />
                            <Text>Javascript</Text>
                            <Image style = {styles.iconSmall} source = {require('./assets/star.png')}/>
                            <Image style = {styles.iconSmall} source = {require('./assets/star.png')}/>
                            <Image style = {styles.iconSmall} source = {require('./assets/star.png')}/>
                            <Image style = {styles.iconSmall} source = {require('./assets/starBlank.png')}/>
                            <Image style = {styles.iconSmall} source = {require('./assets/starBlank.png')}/>
                        </View>
                    </View>
                </View>
                <View style={styles.container}>
                    <Button title="KELUAR" />
                </View>
            </View>
        {/* </AuthProvider> */}
        </>
    );
}

const colors = {
    backgroundSilver : '#EFEFEF',
    backgroundWhite : 'white',
    lightBlue: "#3EC6FF",
    darkBlue: "#003366"

}

const styles = StyleSheet.create({
    containerUtama : {
        backgroundColor : colors.backgroundSilver,
    },
    containerContent     : {
        flexDirection : 'column',
        alignItems : 'flex-start',
        backgroundColor : colors.backgroundWhite,
        paddingVertical : 15,
        paddingHorizontal : 15,
        marginVertical : 15,
        marginHorizontal : 15
    },
    containerDetail     : {
        flexDirection : 'column',
        alignItems : 'flex-start',
        backgroundColor : colors.backgroundWhite,
        borderColor : 'black',
        borderWidth : 2,
        paddingVertical : 15,
        paddingHorizontal : 15,
        marginVertical : 15,
        marginHorizontal : 15
    },
    containerName : {
        flexDirection : 'row', 
    },
    containerIcon : {
        flexDirection : 'row'
    },
    textOuter : {
        paddingLeft : 25
    },
    textInput : {
        borderColor : colors.backgroundSilver,
        borderWidth : 2,
        marginHorizontal : 5,
        marginVertical : 5,
        padding : 10,
        height : 45,
        width : 300
    },
    textButtonRegister : {
        color : 'white',
        fontWeight : 'bold',
        marginVertical : 12
    },
    textButtonLogin : {
        color : colors.lightBlue,
        fontWeight : 'bold',
        marginVertical : 12
    },
    textTitle : {
        fontSize : 35,
        fontWeight : '600',
        color : 'black',
        paddingHorizontal : 10
    },
    textNotes : {
        fontSize : 12,
        fontWeight : '600',
        padding : 5
    },
    buttonRegister : {
        height : 50,
        width : 300,
        backgroundColor : colors.lightBlue,
        borderColor : colors.lightBlue,
        borderWidth : 2,
        marginVertical : 10,
    },
    buttonLogin : {
        height : 50,
        width : 300,
        backgroundColor : 'white',
        borderColor : colors.lightBlue,
        borderWidth : 2,
        marginVertical : 10
    },
    icon : {
        // height : 200,
        width : 50,
        height : 50
        // resizeMode : 'contain'
    },
    iconSmall : {
        // height : 200,
        width : 25,
        height : 25
        // resizeMode : 'contain'
    },
    containerButtonRegister : {
        flexDirection : 'column',
        alignItems : 'center',
        justifyContent : 'center'
    },
    containerButtonLogin : {
        flexDirection : 'column',
        alignItems : 'center',
        justifyContent : 'center'
    }
});