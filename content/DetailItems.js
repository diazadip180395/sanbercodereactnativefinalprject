import React, { useEffect, useState, useContext } from 'react'
import { StyleSheet, Text, View, Image, Button, FlatList, ScrollView } from 'react-native'
import { ItemsGetDetail } from './ApiItems';
import { AuthContext } from './AuthProvider';

export default function DetailItem({ route, navigation }) {
    const {
        username, setUsername,
        idSelected, setIdSelected,
        totalPrice, setTotalPrice } = useContext(AuthContext);

    const [isLoading, setIsLoading] = useState(false);

    const [items, setItems] = useState([]);

    const fetchItems = async () => {
        try {
            // setIsLoading(true);

            const res = await ItemsGetDetail(idSelected);
            const _items = res.data;
            console.log("res: ", _items);

            setItems(_items);
        } catch (error) {
            handleError(error);
        } finally {
			setIsLoading(true);
		}
    };

    useEffect(() => {
        fetchItems()
    }, []);

    const currencyFormat = (num) => {
        return '$ ' + parseInt(num).toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    };

    const updateHarga = (price) => {
        console.log("UpdatPrice : " + price);
        const temp = Number(price) + totalPrice;
        console.log(temp)
        setTotalPrice(temp)
    }

    return (
        
        <View style={styles.container}>
            <View style={{ flexDirection: 'row', justifyContent: "space-between", padding: 16 }}>
                <View>
                    <Text>Selamat Datang,</Text>
                    <Text style={{ fontSize: 18, fontWeight: 'bold' }}>{username}</Text>
                </View>
                <View>
                    <Text>Total Harga:</Text>
                    <Text style={{ fontSize: 18, fontWeight: 'bold' }}> {currencyFormat(totalPrice)}</Text>
                </View>
            </View>
            {isLoading ? 
            <ScrollView style={{ padding: 10 }}>
                <View>
                    <View >
                        <Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 35 }}>{`${items.title} \n`}</Text>
                        <Image
                            style={styles.image}
                            source={{ uri: items.image }}
                        />
                        <Text style={{ textAlign: 'justify' }}>Desc{`\t`}: {`\n`}{items.description}{`\n`}</Text>
                        <Text>Category{`\t`}: {items.category}{`\n`}</Text>
                        <Text>Price{`\t\t\t`}: {currencyFormat(items.price)},-</Text>
                    </View>
                    <View style={{ alignItems: 'stretch' }}>
                        <Button
                            title="Tambah Ke Keranjang"
                            onPress={() => { updateHarga(items.price) }}
                        />
                        <Text></Text>
                    </View>
                </View> 
            </ScrollView> : <Text>Sedang Mengambil Data . . .</Text>}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    content: {
        width: 150,
        height: 220,
        margin: 5,
        borderWidth: 1,
        alignItems: 'center',
        borderRadius: 5,
        borderColor: 'grey',
    },
    image: {
        width: 250,
        height: 250,
    },
    list:
    {
        borderWidth: 1,
        margin: 5,
        padding: 5,
        width: 160
    },
})
