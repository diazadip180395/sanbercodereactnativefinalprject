import React, { useContext, useEffect, useState } from "react";
import { Image, StyleSheet, Text, View, TextInput, Button } from "react-native";
import { AuthProvider, AuthContext } from "./AuthProvider";

export default function Login({ navigation }) {
  const { isLoggedIn, setIsLoggedIn, username, setUsername, password, setPassword } = useContext(AuthContext);
  const [isError, setIsError] = useState(false);

  const submit = () => {
    navigation.navigate("MainApp", { username: username })
  };
  return (
    <>
        <View style={styles.container}>
          <Text style={{ fontSize: 20, fontWeight: "bold" }}>Diaz's Store</Text>
          <Image
            style={{ height: 150, width: 150 }}
            source={require("./assets/cart.jpg")}
          />
          <View>
            <TextInput
              style={{
                borderWidth: 1,
                paddingVertical: 10,
                borderRadius: 5,
                width: 300,
                marginBottom: 10,
                paddingHorizontal: 10,
              }}
              placeholder="Masukan Username"
              value={username}
              onChangeText={(value) => setUsername(value)}
            />
            <TextInput
              style={{
                borderWidth: 1,
                paddingVertical: 10,
                borderRadius: 5,
                width: 300,
                marginBottom: 10,
                paddingHorizontal: 10,
              }}
              placeholder="Masukan Password"
              value={password}
              onChangeText={(value) => setPassword(value)}
            />
            <Button onPress={submit} title="Login" />
          </View>
        </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center",
  },
});
