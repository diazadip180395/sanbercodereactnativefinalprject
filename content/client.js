import axios from "axios";
export const baseURL = "https://fakestoreapi.com/";

export const client = axios.create({
	baseURL: baseURL,
});