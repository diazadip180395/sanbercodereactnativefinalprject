import React from 'react'
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import 'react-native-gesture-handler';
import Login from './Login';
import Home from './Home'

import AboutScreen from './AboutScreen'
import DetailItem from './DetailItems';

import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";



const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

export default function Index() {
    return (
       <NavigationContainer>
        <Stack.Navigator initialRouteName="Login" >
          <Stack.Screen name='Login' component={Login} options={{ headerShown: false }} />
          <Stack.Screen name="MainApp" component={MainApp}/>
          <Stack.Screen name="DetailItem" component={DetailItem}/>
        </Stack.Navigator>
      </NavigationContainer>
    )
}

const MainApp = () => (
  <Tab.Navigator screenOptions={{ headerShown: false, tabBarIconStyle: { display: "none" } }}>
    <Tab.Screen name="Home" component={Home}/>
    <Tab.Screen name="AboutScreen" component={AboutScreen}/>
  </Tab.Navigator>
)
