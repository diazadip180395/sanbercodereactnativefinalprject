import React, { useEffect, useState, useContext } from 'react'
import { StyleSheet, Text, View, Image, Button, FlatList } from 'react-native'
import {ItemsGet} from './ApiItems'
import { AuthContext } from './AuthProvider';

export default function Home({route, navigation}) {  
    const { isLoggedIn, setIsLoggedIn, username, setUsername, password, setPassword, idSelected, setIdSelected } = useContext(AuthContext);
    const [isLoading, setIsLoading] = useState(false);
	const [items, setItems] = useState([]);

    const fetchItems = async () => {
            try {
                setIsLoading(true);
    
                const res = await ItemsGet();
                const _items = res.data;
                console.log("res: ", _items);
    
                setItems(_items);
            } catch (error) {
                handleError(error);
            } finally {
                setIsLoading(false);
            }
	};

    useEffect(() => {
		fetchItems()
	}, []);
    
    // const { username } = route.params;
    // const username = "Guest";
    const [totalPrice, setTotalPrice] = useState(0);

    const currencyFormat=(num)=> {
        return '$ ' + parseInt(num).toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
      };

    const updateHarga =(price)=>{
        console.log("UpdatPrice : " + price);
        const temp = Number(price) + totalPrice;
        console.log(temp)
        setTotalPrice(temp)
        
        //? #Bonus (10 poin) -- HomeScreen.js --
        //? agar harga dapat update misal di tambah lebih dari 1 item atau lebih -->
            
    }

    const Item = ({ title }) => (
        <View style={styles.item}>
          <Text style={styles.title}>{title}</Text>
        </View>
      );
          
    const renderItem = ({ item }) => (
        <>
            <View style={styles.list}>
                <Text style={{fontWeight:'bold', textAlign:'center'}}>{item.title}</Text>
                <Image
                    style={styles.image}
                    source={{uri:item.image}} 
                />
                <Text>Desc : {item.description}{`\n`}</Text>
                <Text>Type : {item.price}{`\n`}</Text>
                <Text>{currencyFormat(item.price)},-</Text>
                <Button
                    title="Tambah Ke Keranjang"
                    onPress={() => {updateHarga(item.price)}}
                />
            </View>
        </>
      );

    return (
        <View style={styles.container}>
            <View style={{flexDirection:'row', justifyContent:"space-between", padding: 16}}>
                <View>
                    <Text>Selamat Datang,</Text>
                    <Text style={{fontSize:18, fontWeight:'bold'}}>{username}</Text>
                </View>
                <View>
                    <Text>Total Harga:</Text>
                    <Text style={{fontSize:18, fontWeight:'bold'}}> {currencyFormat(totalPrice)}</Text>
                </View>
            </View>
            <View style={{alignItems:'center',  marginBottom: 20, paddingBottom: 60}}>
                <FlatList
                    data={items}
                    renderItem={renderItem}
                    keyExtractor={item => item.id}
                    numColumns={2}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,        
        backgroundColor:'white', 
    },  
    content:{
        width: 150,
        height: 220,        
        margin: 5,
        borderWidth:1,
        alignItems:'center',
        borderRadius: 5,
        borderColor:'grey',    
    },
    image:{
        width:100,
        height:100,
    },
    list:
    {
        borderWidth : 1,
        margin : 5,
        padding : 5,
        width : 160
    },        
})
