import React, { useEffect, useState, useContext } from 'react'
import { StyleSheet, Text, View, Image, Button, FlatList } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { ItemsGet } from './ApiItems'
import { AuthContext } from './AuthProvider';

export default function Home({ route, navigation }) {
    const { 
        username, setUsername,
        idSelected, setIdSelected,
        totalPrice, setTotalPrice } = useContext(AuthContext);
    const [isLoading, setIsLoading] = useState(false);
    const [items, setItems] = useState([]);

    const fetchItems = async () => {
        try {
            // setIsLoading(true);

            const res = await ItemsGet();
            const _items = res.data;
            console.log("res: ", _items);

            setItems(_items);
        } catch (error) {
            handleError(error);
        } finally {
			setIsLoading(true);
		}
    };

    useEffect(() => {
        fetchItems()
    }, []);

    const currencyFormat = (num) => {
        return '$ ' + parseInt(num).toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    };

    const updateHarga = (price) => {
        console.log("UpdatPrice : " + price);
        const temp = Number(price) + totalPrice;
        console.log(temp)
        setTotalPrice(temp)
    }

    const setId = (id) => {
        setIdSelected(id)
        navigation.navigate("DetailItem")
    }

    const Item = ({ title }) => (
        <View style={styles.item}>
            <Text style={styles.title}>{title}</Text>
        </View>
    );

    const renderItem = ({ item }) => (
        <>
            <View>
                <TouchableOpacity onPress={() => { setId(item.id) }} style={styles.list}>
                    <View >
                        <Text style={{ fontWeight: 'bold', textAlign: 'center' }}>{item.title}</Text>
                        <Image
                            style={styles.image}
                            source={{ uri: item.image }}
                        />
                        <Text>Category{`\t`}: {item.category}{`\n`}</Text>
                        <Text>Price{`\t\t\t`}: {currencyFormat(item.price)},-</Text>
                    </View>
                </TouchableOpacity>
                <View style={{ width: 175, padding: 5 }}>
                    <Button
                        title="Tambah Ke Keranjang"
                        onPress={() => { updateHarga(item.price) }}
                    />
                </View>
            </View>
        </>
    );

    return (
        <View style={styles.container}>
            <View style={{ flexDirection: 'row', justifyContent: "space-between", padding: 16 }}>
                <View>
                    <Text>Selamat Datang,</Text>
                    <Text style={{ fontSize: 18, fontWeight: 'bold' }}>{username}</Text>
                </View>
                <View>
                    <Text>Total Harga:</Text>
                    <Text style={{ fontSize: 18, fontWeight: 'bold' }}> {currencyFormat(totalPrice)}</Text>
                </View>
            </View>
            {isLoading ? 
            <View style={{ alignItems: 'stretch', marginBottom: 20, paddingBottom: 60 }}>
                <FlatList
                    data={items}
                    renderItem={renderItem}
                    keyExtractor={item => item.id}
                    numColumns={2}
                />
            </View>
            : <Text>Sedang Mengambil Data . . .</Text>}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    content: {
        width: 150,
        height: 220,
        margin: 5,
        borderWidth: 1,
        alignItems: 'center',
        borderRadius: 5,

    },
    image: {
        width: 100,
        height: 100,
    },
    list:
    {
        borderWidth: 1,
        margin: 5,
        padding: 5,
        width: 170,
        height: 250,
    },
})
